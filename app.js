// Person class
class Person {
  constructor(name, age, salary, sex) {
    this.name = name;
    this.age = age;
    this.salary = salary;
    this.sex = sex;
  }
  static sort(arr, field, order) {
    // if array length is less than one then return arr
    if (arr.length <= 1) {
      return arr;
    }

    // seperate arrays of elements to the left and right of pivot element
    const right = [];
    const left = [];

    // pivot element
    const pivot = arr[arr.length - 1][field];

    // according to the order push the items into the particular arrays
    for (const item of arr.slice(0, arr.length - 1)) {
      if (order === "asc") {
        item[field] > pivot ? right.push(item) : left.push(item);
      } else if (order === "desc") {
        item[field] > pivot ? left.push(item) : right.push(item);
      }
    }

    // by using recurssion the sort the left and right array
    // and then return the (left sorted + pivot + right sorted array)
    return [
      ...this.sort(left, field, order),
      arr[arr.length - 1],
      ...this.sort(right, field, order),
    ];
  }
  w;
}

// Initialized persons
const person1 = new Person("Anish", 18, 10000, "Male");
const person2 = new Person("Smita", 40, 100000, "Female");
const person3 = new Person("Randhir", 55, 50000, "Male");
const person4 = new Person("Anuj", 12, 13230, "Male");
const person5 = new Person("Siddhant", 19, 10234234, "Male");
const person6 = new Person("Anay", 20, 12230, "Male");
const person7 = new Person("Mehavi", 17, 1450, "Male");
const person8 = new Person("Archana", 38, 5000, "Male");

// Person array
const personArr = [
  person1,
  person2,
  person3,
  person4,
  person5,
  person6,
  person7,
  person8,
];

// Sorted array
const sortedArr = Person.sort(personArr, "age", "asc");

console.log(sortedArr);
